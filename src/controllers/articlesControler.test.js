import { expect } from 'chai';
import sinon from 'sinon';
import articlesController from './articlesController';

describe('Articles Controller Tests:', () => {
  describe('Post', () => {
    it('should not allow an empty code on post', () => {
      const Article = function (article) { // eslint-disable-line func-names
        this.save = (callback) => { callback(); };
        this.article = article;
      };

      const req = {
        body: {
          description: 'testing article',
        },
      };

      const res = {
        status: sinon.spy(),
        send: sinon.spy(),
        json: sinon.spy(),
      };

      const controller = articlesController(Article);
      controller.post(req, res);

      expect(res.status.calledWith(400)).to
        .equal(true, `Bad status ${res.status.args[0][0]}`);
    });
  });
});
