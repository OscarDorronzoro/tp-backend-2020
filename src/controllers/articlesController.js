function articlesController(Article) {
  function post(req, res) {
    const article = new Article(req.body);

    if (!article.code) {
      res.status(400);
      res.send('Code is required');
    } else {
      article.save((err) => {
        if (err) {
          return res.send(err);
        }
        res.status(201);
        return res.json(article);
      });
    }
  }

  function get(req, res) {
    let query = {};
    if (req.query.code) {
      query.code = req.query.code;
    }
    if (req.query.category) {
      query = { 'category.name': req.query.category };
    }

    Article.find(query, (err, articles) => {
      if (err) {
        return res.send(err);
      }
      const returnArticles = articles.map((article) => {
        const newArticle = article.toJSON();
        newArticle.links = {};
        newArticle.links.self = `http://${req.headers.host}/api/articles/${article._id}`; // eslint-disable-line no-underscore-dangle
        return newArticle;
      });
      return res.json(returnArticles);
    });
  }

  return { post, get };
}

module.exports = articlesController;
