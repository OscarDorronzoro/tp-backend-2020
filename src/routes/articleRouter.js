import express from 'express';
import mongoose from 'mongoose';
import reAssign from '../utils/reAssign';
import articleController from '../controllers/articlesController';

function routes(Article) {
  const articleRouter = express.Router();
  const controller = articleController(Article);

  articleRouter.route('/articles')
    .post(controller.post)
    .get(controller.get);

  articleRouter.use('/articles/:articleId', (req, res, next) => {
    Article.findById(req.params.articleId, (err, article) => {
      if (err) {
        return res.send(err);
      }
      if (article) {
        req.article = article;
        return next();
      }
      return res.sendStatus(404);
    });
  });

  articleRouter.route('/articles/:articleId')
    .get((req, res) => {
      const { article } = req;

      const returnArticle = article.toJSON();
      returnArticle.links = {};
      const category = article.category.name.replace(' ', '%20');
      returnArticle.links.filterByThisCategory = `http://${req.headers.host}/api/articles/?category=${category}`;

      res.json(returnArticle);
    })
    .put((req, res) => {
      const { article } = req;
      reAssign(article, req.body, mongoose.model('Article').schema);
      article.save((err) => {
        if (err) {
          return res.send(err);
        }
        return res.json(article);
      });
    })
    .patch((req, res) => {
      const { article } = req;

      if (req.body._id) { // eslint-disable-line no-underscore-dangle
        delete req.body._id; // eslint-disable-line no-underscore-dangle
      }
      Object.assign(article, req.body);
      article.save((err) => {
        if (err) {
          return res.send(err);
        }
        return res.json(article);
      });
    })
    .delete((req, res) => {
      req.article.remove((err) => {
        if (err) {
          return res.send(err);
        }
        return res.sendStatus(204);
      });
    });

  return articleRouter;
}

module.exports = routes;
