import mongoose, { Schema } from 'mongoose';

const costumerModel = new Schema(
  {
    username: { type: String },
    name: { type: String },
    lastName: { type: String },
    password: { type: String },
    dni: { type: String },
    compras: { type: Array },
  },
);

module.exports = mongoose.model('Costumer', costumerModel);
