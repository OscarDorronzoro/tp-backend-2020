import mongoose, { Schema } from 'mongoose';

const articleModel = new Schema(
  {
    code: { type: String },
    description: { type: String },
    category: {
      name: { type: String },
      description: { type: String },
    },
    urlImage: { type: String, default: 'images/no_image.png' },
    stock: { type: Number, default: 0 },
    amountToOrder: { type: Number },
    orderPoint: { type: Number },
    price: {
      value: { type: Number },
      sinceDate: { type: Date },
    },
    providers: { type: Array, default: [] },
  },
);

module.exports = mongoose.model('Article', articleModel);
