require('@babel/register'); // this file is not being transpiled
const { MongoClient } = require('mongodb');
const assert = require('assert');
const dataToInsert = require('../DB/MongoDB/cleanning_supplies_testing/importTestingDataBase_v1.00');

process.env.ENV = 'Test';

const url = 'mongodb://localhost:27017';
const dbName = 'cleanning_supplies_testing';

MongoClient.connect(url, (err, client) => {
  assert.strictEqual(null, err);

  const db = client.db(dbName);
  const articles = db.collection('articles');
  articles.deleteMany({});
  articles.insertMany(dataToInsert.articles);

  const costumers = db.collection('costumers');
  costumers.deleteMany({});
  costumers.insertMany(dataToInsert.costumers);

  client.close();
});
