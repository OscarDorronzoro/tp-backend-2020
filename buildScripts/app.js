import express from 'express';
import chalk from 'chalk';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import Article from '../src/models/articleModel';

/* eslint-disable no-console */

const app = express();
const port = process.env.PORT || 3000;

if (process.env.ENV === 'Test') {
  console.log(chalk.blue('This is the testing database'));
  mongoose.connect(
    'mongodb://localhost/cleanning_supplies_testing',
    { useNewUrlParser: true, useUnifiedTopology: true }, // it's due to a deprecation warning
  );
} else {
  console.log(chalk.yellow('This is the devolopment database'));
  mongoose.connect(
    'mongodb://localhost/cleanning_supplies_development',
    { useNewUrlParser: true, useUnifiedTopology: true }, // it's due to a deprecation warning
  );
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const articleRouter = require('../src/routes/articleRouter')(Article);

app.use('/api', articleRouter);

app.get('/', (req, res) => {
  res.send('Welcome to Cleanning Supplies API, you can see details on gitlab');
});

app.server = app.listen(port, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log(chalk.green(`Running on port ${port}`));
  }
});

module.exports = app;
